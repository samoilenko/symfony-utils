<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 9/13/15
 * Time: 11:05 PM
 */

namespace SymfonyUtils\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class TestJsonValidator extends ConstraintValidator {
    public function validate($value, Constraint $constraint){
        if (false === json_decode($value)) {
            $this->context->addViolation($constraint->message);

            return false;
        }

        return true;
    }
}