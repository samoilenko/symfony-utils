<?php
/**
 * Created by IntelliJ IDEA.
 * User: samoilenko
 * Date: 9/13/15
 * Time: 11:05 PM
 */
namespace SymfonyUtils\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */

class TestJson extends Constraint {

    public $message = 'NOT_VALID_JSON';

    public function validatedBy(){
        return 'validator.testJson';
    }
} 