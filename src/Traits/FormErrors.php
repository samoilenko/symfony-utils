<?php
declare(strict_types=1);
namespace SymfonyUtils\Traits;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Form;

trait FormErrors {
    public function getFormErrors(FormInterface $form): array{
        $errors = [$this->prepareMessage((string)$form->getErrors())];
        foreach($form as $child){
            if($child->getErrors()->count() > 0){
                $errors[] = $this->prepareMessage((string)$child->getErrors());
            }
        }

        return $errors;
    }

    private function prepareMessage(string $message): string{
        return str_replace("ERROR: ", "", $message);
    }

    protected function removeExtraFields($requestData, $form): array{
        return array_intersect_key($requestData, $form->all());
    }

    protected function getFormFieldsAsArray(FormInterface $form): array{
        $result = [];
        /** @var Form $child */
        foreach($form->all() as $k => $child){
            $result[$k] = $child->getData();
        }

        return $result;
    }
}